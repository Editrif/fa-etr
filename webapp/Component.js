sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"zfaetr/Z_FA_ETR/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("zfaetr.Z_FA_ETR.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			// this.setModel(models.createDeviceModel(), "device");
			// this.setModel(models.createMyModelModel(), "myModel");
			// this.setModel(models.createGermanModel(), "myGerman");
			// this.setModel(models.createChoisesModel(), "choice");
			this.setModel(models.createDataModel(), "data");
		
		}
	});
});