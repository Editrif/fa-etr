sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		// createDeviceModel: function () {
		// 	var oModel = new JSONModel(Device);
		// 	oModel.setDefaultBindingMode("OneWay");
		// 	return oModel;
		// },
		// createMyModelModel: function () {
		// 	var myData = {
		// 		checkNr: 323,
		// 		icon: " "
		// 	};
		// 	var oModel = new JSONModel(myData);

		// 	return oModel;
		// },
		// createGermanModel: function () {
		// 	var myData = {
		// 		name: "",
		// 		gender: "Male",
		// 		age: "10-20",
		// 		faculty: {
		// 			UTCN: true
		// 		},
		// 		hobbies: {
		// 			Gaming: true

		// 		},
		// 		blond: true,
		// 		data: null
		// 	};
		// 	var oModel = new JSONModel(myData);

		// 	return oModel;
		// },
		// createChoisesModel: function () {
		// 	var myData = {

		// 	};
		// 	var oModel = new JSONModel(myData);

		// 	return oModel;
		// },
		createDataModel: function () {
			var oModel = new JSONModel();
			$.get("./model/data.json", function(data) {
				oModel.setProperty("/data", data);
			});
			oModel.setProperty("/setup",{
				dialogAdd: true,
				dialogUpdate: false
			});
			return oModel;
		}
	};
});