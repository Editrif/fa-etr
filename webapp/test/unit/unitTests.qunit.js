/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zfaetr/Z_FA_ETR/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});