/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"zfaetr/Z_FA_ETR/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});